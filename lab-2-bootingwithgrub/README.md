Last week we talked about how the operating systems handle the bootstrap process.
There are a lot of steps in the boot sequence, but we mostly did something on the bootloader ie GRUB in the lab.
On Linux you can read MBR with dd tool.
```
dd if=/dev/sdx bs=512 count=1 | hexdump -C
```

You can look at the [further reading section](https://gitlab.com/CakirHuseyin/CENG_2034_Operating_Systems-2018/tree/master/further-reading#about-system-boot-process) for more theoretical information and documentation.

So assignment is about bootloader.

### Assignment 2:

* Read your MBR and write it into a file.
* Enter a new entry in GRUB with an iso under your home directory.
* Bonus points: Run the new operating system on GRUB entirely on the ram.

### Deadline March 18, 00:00  
#### It does not matter which git host you are using. But I will search in this way: 
#### githost.com/username/CENG_2034_Operating_Systems-2018
#### Make sure you follow this format !!! 

!! Do not forget to share your MBR file in hexadecimal format. (it can be in a text file).
!! it is enough to share the config file you have added the new entry, but it is also nice to add a screenshoot.